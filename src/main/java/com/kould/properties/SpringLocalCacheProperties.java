package com.kould.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("kache.local-cache")
public class SpringLocalCacheProperties extends LocalCacheProperties{
}
