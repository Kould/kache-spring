package com.kould.type;

import com.kould.api.Kache;
import com.kould.entity.PageDetails;
import com.kould.properties.*;

public class KacheSpringBuilder extends Kache.Builder {

    private final SpringDaoProperties daoProperties;

    private final SpringLocalCacheProperties interprocessCacheProperties;

    private final SpringListenerProperties listenerProperties;

    private final SpringKeyProperties keyProperties;

    private final PageDetails<?> pageDetails;

    public KacheSpringBuilder(SpringDaoProperties daoProperties, SpringLocalCacheProperties interprocessCacheProperties, SpringListenerProperties listenerProperties, SpringKeyProperties keyProperties, PageDetails<?> pageDetails) {
        this.daoProperties = daoProperties;
        this.interprocessCacheProperties = interprocessCacheProperties;
        this.listenerProperties = listenerProperties;
        this.keyProperties = keyProperties;
        this.pageDetails = pageDetails;
    }

    @Override
    public Kache build() throws IllegalAccessException {
        return Kache.builder()
                .load(DaoProperties.class, daoProperties)
                .load(LocalCacheProperties.class, interprocessCacheProperties)
                .load(ListenerProperties.class, listenerProperties)
                .load(KeyProperties.class, keyProperties)
                .load(PageDetails.class, pageDetails)
                .build();
    }
}
