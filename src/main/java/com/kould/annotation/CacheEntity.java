package com.kould.annotation;

import com.kould.api.KacheEntity;

import java.lang.annotation.*;

@Inherited
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CacheEntity {
    Class<? extends KacheEntity> value();
}
