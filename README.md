![Logo](https://s3.bmp.ovh/imgs/2022/01/2f5f01726cfef8fb.png)

<p align="middle">
  <a href="https://search.maven.org/artifact/io.gitee.kroup/Kache-Spring">
    <img alt="maven" src="https://img.shields.io/maven-central/v/io.gitee.kroup/Kache-Spring.svg?style=flat-square">
  </a><a href="https://www.apache.org/licenses/LICENSE-2.0">
    <img alt="code style" src="https://img.shields.io/badge/license-Apache%202-4EB1BA.svg?style=flat-square">
  </a>
  <a target="_blank" href="https://www.oracle.com/technetwork/java/javase/downloads/index.html">
      <img src="https://img.shields.io/badge/JDK-11+-red.svg" />
  </a>
</p>

<p align="center">
    <strong>
    Kache-Spring - 散列式缓存 Spring自动配置版
    </strong>
</p>

----

##### 源框架：[Kache: 持久化缓存代理](https://gitee.com/Kould/kache)

##### 使用示例：[Kork:组件示例](https://gitee.com/Kould/kork)

### 使用 | Use

#### **1、Kache依赖引入**

#### 2、缓存实体继承KacheEntity接口实现getPrimaryKey方法

#### 3、Dao层写入注解

#### 4、注册PageDetails对象

##### 示例：

**1**.pom文件引入:

```xml
<dependency>
  <groupId>io.gitee.kroup</groupId>
  <artifactId>Kache-Spring</artifactId>
  <version>1.0.1.INFORMAL_VERSION</version>
</dependency>
```

**2**.缓存实体继承KacheEntity接口

```java
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("kork_article")
public class Article extends BasePO implements KacheEntity {

    private static final long serialVersionUID = -4470366380115322213L;

    @DataFactory(minLen = 10)
    private String title;

    private String summary;

    @JsonAdapter(IdAdapter.class)
    private Long authorId;

    @JsonAdapter(IdAdapter.class)
    private Long bodyId;

    @JsonAdapter(IdAdapter.class)
    private Long categoryId;

    private Integer commentCounts;

    private Long viewCounts;

    private ArticleType weight;

    @Override
    public String getPrimaryKey() {
        return getId().toString();
    }
}
```



**3**.其对应的**Dao层**的Mapper以及Dao方法添加注释：

- 持久化方法注解：@DaoMethod
  - Type：方法类型：
    - value = Type.SELECT : 搜索方法
    - value = Type.INSERT : 插入方法
    - value = Type.UPDATE : 更新方法
    - value = Type.DELETE : 删除方法
  - Status：方法参数状态 默认为Status.BY_Field：
    - status = Status.BY_FIELD : 非ID查询方法
    - status = Status.BY_ID : ID查询方法
  - Class<?>[] involve：仅在Type.SELECT Status.BY_Field时生效：用于使该条件搜索方法的索引能够被其他缓存Class影响

```java
@Repository
// 自动注册代理注解
@CacheEntity(Article.class)
public interface TagMapper extends BaseMapper<Tag> {

    @Select("select t.* from klog_article_tag at "
            + "right join klog_tag t on t.id = at.tag_id "
            + "where t.deleted = 0 AND at.deleted = 0 "
            + "group by t.id order by count(at.tag_id) desc limit #{limit}")
    @DaoMethod(value = Type.SELECT,status = Status.BY_FIELD)
    // 通过条件查询获取数据
    List<Tag> listHotTagsByArticleUse(@Param("limit") int limit);

    @DaoMethod(Type.INSERT)
    // 批量新增方法（会导致数据变动）
    Integer insertBatch(Collection<T> entityList);
}
```

**4**.注册PageDetails对象

构造参数：

1. 分页对象Class：Page.class

2. 实体数据集合属性名：“records”

3. 实体数据集合属性Class：List.class

```java
@Configuration
public class KacheConfig {
    @Bean
    public PageDetails<Page> pageDetails() throws NoSuchFieldException, IllegalAccessException {
        return new PageDetails<>(Page.class, "records", List.class);
    }
}
```

自定义配置或组件：

```java
// 以接口类型作为键值替换默认配置或增加额外配置
// 用于配置或组件加载
load(Class<?> interfaceClass, Object bean);

// 示例:使用Kache默认提供的额外AMQP异步删改策略实现
@Bean
public Kache kache() {
    return Kache.builder()
            // 新增Connection接口配置,并提供接口实例
            .load(Connection.class, factory.newConnection())
            .build();
}
```

application.yml参考配置

```yaml
#Kache application.yml对应属性值
kache:
   dao:
       base-time: 86400 // 缓存基本存活时间
       random-time: 600 // 缓存随机延长时间
       poolMaxTotal: 20 // Redis连接池最大连接数
       poolMaxIdle: 5   // Redis连接池最大Idle状态连接数
       casKeepTime: 1   // 幂等cas凭证删除时间
   local-cache:
       enable: true // 进程间缓存是否开启
       size: 50     // 进程间缓存数量
   listener:
       enable: true  // 监听器是否开启
   method-key:  // dao方法名匹配（适用于无法添加注释情况）
       selectKey: "select"
       insertKey: "insert"
       deleteKey: "delete"
       updateKey: "update"
       selectByIdKey: "selectById"
       insertByIdKey: "insertById"
       updateByIdKey: "updateById"
       deleteByIdKey: "deleteById"
       
```
